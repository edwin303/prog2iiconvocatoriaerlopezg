﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using Prog2IIConvocatoriaerlopezg.Models;
using Prog2IIConvocatoriaerlopezg.Views.UserControls;
using System.Windows;
using System.Windows.Controls;

namespace Prog2IIConvocatoriaerlopezg.Controllers
{
    class PersonaController
    {
        PersonForm pwindow;
        SaveFileDialog Guardardialog;
        OpenFileDialog Abrirdialog;

        public PersonaController(PersonForm form)
        {
            pwindow = form;
            Guardardialog = new SaveFileDialog();
            Abrirdialog = new OpenFileDialog();
        }
        public void PersonEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "GuardarButton":
                    GuardarDatos();
                    break;
                case "AbrirButton":
                    AbrirFile();
                    break;
            }
        }

        private void AbrirFile()
        {
            Abrirdialog.Filter = "Json File (*.json)|*.json";
            if (Abrirdialog.ShowDialog() == true)
            {
                Persona p = new Persona();
                pwindow.SetData(p.FromJson(Abrirdialog.FileName));

            }
        }

        private void GuardarDatos()
        {
            Guardardialog.Filter = "Json File (*.json)|*.json";
            if (Guardardialog.ShowDialog() == true)
            {
                Persona p;

                p = pwindow.GetData();

                p.ToJson(Guardardialog.FileName);
            }
        }

        private void SaveCollection()
        {
            Guardardialog.Filter = "Json File (*.json)|*.json";
            if (Guardardialog.ShowDialog() == true)
            {
                Group g = new Group
                {
                    Name = "Grupo de demostración",
                    Members = new List<Persona>
                    {
                        new Persona() { Nombres = "William", Apellidos = "Sanchez", Nacimiento = new System.DateTime(1983, 9, 1), Departamento = "Masaya" }
                    }
                };

                Persona p = new Persona() { Nombres = "Douglas", Apellidos = "Sanchez", Nacimiento = new System.DateTime(1982, 08, 17), Departamento = "Masaya" };
                g.Members.Add(p);

                g.Members.Add(new Persona() { Nombres = "Erick", Apellidos = "Sanchez", Nacimiento = new System.DateTime(1984, 9, 1), Departamento = "Masaya" });
                g.Members.Add(new Persona() { Nombres = "Carolina", Apellidos = "Sanchez", Nacimiento = new System.DateTime(1985, 6, 8), Departamento = "Masaya" });
                g.Members.Add(new Persona() { Nombres = "Ricardo", Apellidos = "Sanchez", Nacimiento = new System.DateTime(1988, 5, 6), Departamento = "Masaya" });

                g.ToJson(Guardardialog.FileName);
            }
        }
    }
}
}
