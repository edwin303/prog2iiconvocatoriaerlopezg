// Updated by XamlIntelliSenseFileGenerator 15/07/2021 15:49:16
#pragma checksum "..\..\..\..\..\Views\UserControls\PersonForm.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3266A30DA45A89E1C9CF5EA3F39F74110B0DAD7C"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Prog2IIConvocatoriaerlopezg.Views.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Prog2IIConvocatoriaerlopezg.Views.UserControls
{


    /// <summary>
    /// PersonForm
    /// </summary>
    public partial class PersonForm : System.Windows.Window, System.Windows.Markup.IComponentConnector
    {

#line default
#line hidden


#line 14 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label NacimientoLabel;

#line default
#line hidden


#line 15 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker NacimientoPicker;

#line default
#line hidden


#line 16 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label DepartamentoLabel;

#line default
#line hidden


#line 17 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox DepartamentoCombobox;

#line default
#line hidden


#line 31 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GuardarButton;

#line default
#line hidden


#line 32 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AbrirButton;

#line default
#line hidden

        private bool _contentLoaded;

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.4.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Prog2IIConvocatoriaerlopezg;V1.0.0.0;component/views/usercontrols/personform.xam" +
                    "l", System.UriKind.Relative);

#line 1 "..\..\..\..\..\Views\UserControls\PersonForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.4.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
        {
            switch (connectionId)
            {
                case 1:
                    this.PrimerNombreLabel = ((System.Windows.Controls.Label)(target));
                    return;
                case 2:
                    this.PrimerNombreTextBox = ((System.Windows.Controls.TextBox)(target));
                    return;
                case 3:
                    this.SegundoNombreLabel = ((System.Windows.Controls.Label)(target));
                    return;
                case 4:
                    this.SegundoNombreTextBox_Copy = ((System.Windows.Controls.TextBox)(target));
                    return;
                case 5:
                    this.NacimientoLabel = ((System.Windows.Controls.Label)(target));
                    return;
                case 6:
                    this.NacimientoPicker = ((System.Windows.Controls.DatePicker)(target));
                    return;
                case 7:
                    this.DepartamentoLabel = ((System.Windows.Controls.Label)(target));
                    return;
                case 8:
                    this.DepartamentoCombobox = ((System.Windows.Controls.ComboBox)(target));
                    return;
                case 9:
                    this.GuardarButton = ((System.Windows.Controls.Button)(target));
                    return;
                case 10:
                    this.AbrirButton = ((System.Windows.Controls.Button)(target));
                    return;
            }
            this._contentLoaded = true;
        }

        internal System.Windows.Controls.Label NombresLabel;
        internal System.Windows.Controls.TextBox NombresTextBox;
        internal System.Windows.Controls.Label ApellidosLabel;
        internal System.Windows.Controls.TextBox ApellidosTextBox;
    }
}

