﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prog2IIConvocatoriaerlopezg.Models
{
    class Persona
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime Nacimiento { get; set; }
        public int Edad { get { return Nacimiento.YearsFrom(); } }
        public string Departamento { get; set; }

        public Persona FromJson(string Path)
        {
            return JsonSerialization.ReadFromJsonFile<Persona>(Path);
        }

        public void ToJson(string Path)
        {
            JsonSerialization.WriteToJsonFile(Path, this, false);
        }
    }
}
}
