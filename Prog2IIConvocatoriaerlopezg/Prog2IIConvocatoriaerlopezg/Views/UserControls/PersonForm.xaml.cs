﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Prog2IIConvocatoriaerlopezg.Models;

namespace Prog2IIConvocatoriaerlopezg.Views.UserControls
{
    /// <summary>
    /// Lógica de interacción para PersonForm.xaml
    /// </summary>
    public partial class PersonForm : Window
    {
        public PersonForm()
        {
            InitializeComponent();
        }

        public Persona GetData()
        {
            Persona person = new Persona
            {
                Nombres = NombresTextBox.Text,
                Apellidos = ApellidosTextBox.Text,
                Nacimiento = (DateTime)NacimientoPicker.SelectedDate,
                Departamento = DepartamentoCombobox.SelectedValue.ToString()
            };
            return person;
        }

        public void SetData(Persona data)
        {
            NombresTextBox.DataContext = data;
            ApellidosTextBox.DataContext = data;
            NacimientoPicker.DataContext = data;
            DepartamentoCombobox.DataContext = data;
        }

        protected void SetupController()
        {
            pc = new PersonController(this);
            this.GuardarButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.AbrirButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
        }
        public void Hide()
        {
            this.Visibility = Visibility.Collapsed;
        }
    }
}
}
